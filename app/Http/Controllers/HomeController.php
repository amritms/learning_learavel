<?php

namespace App\Http\Controllers;

Class HomeController extends Controller{

    public function __construct()
    {

    }
    public function contact()
    {
        return view('pages.contact');
    }
}