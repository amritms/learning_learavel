<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use Log;
class ArticlesController extends Controller
{
    public function index()
    {
//        Log::info('This is some useful information.');
//        $monolog = Log::getMonolog();
//        dd($monolog);
        $articles = Article::latest('published_at')->get();
        return view('articles.index', compact('articles'));
    }

    public function show( $id )
    {
        try{
            $article = Article::FindOrFail($id);
            Log::info('article viewed',
                ['article id' => $id]
            );
            return view('articles.show', compact('article'));
        }catch(Exception $e){
            Log::info(sprintf("%s couldn't be found", $e->getMessage()),
                [
                    'article id:' => $id,
                    'trace' => $e->getTraceAsString()
                ]
            );
        }

    }

    public function calc($a=0, $b=0)
    {
        return $a + $b;
    }

    public function create()
    {
        return view('articles.create');
    }

    public function store()
    {
        $input = Request::all();
        $input['published_at'] = Carbon::now();
        Article::create($input);
        return redirect('articles');
    }
}
