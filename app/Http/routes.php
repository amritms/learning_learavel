<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('contact',  'PagesController@contact');
Route::get('about',  'PagesController@about');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);

Route::get('articles/create', 'ArticlesController@create');
Route::get('articles/calc', 'ArticlesController@calc');
Route::get('articles/{id}', 'ArticlesController@show');
Route::get('articles', 'ArticlesController@index');
Route::post('articles', 'ArticlesController@store');
