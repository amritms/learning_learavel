<!doctype html>
<html lang="en">
<head>
    <script type="text/javascript" src="{{asset("../resources/assets/js/newrelic.min.js")}}"></script>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="{{asset("../resources/assets/js/le.min.js")}}"></script>
</head>
<body>
<div class="container">
    @yield('content')
</div>

@yield('footer')
</body>
</html>